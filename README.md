Projet training center
Projet pour pouvoir gerer les informations de formations : leurs eleves, leurs formateurs et leurs salles.

Lien Trello avec les diagrammes et worklfow : https://trello.com/invite/b/JRSrFqo7/ATTI55c8f8f921813a4bdaf9473a73a55e66748654CE/projet-centre-de-formation 

Notre projet a comme classes :
- Student : les eleves du centre de formation. Un eleve possède une formation (id course). 
- Course : les formations. Une formation possède une salle (id room). Et elle est aussi lié via une table de jointure (Course_teacher) à un ou plusieurs formateurs.
- Teacher : les formateurs. Un formateur est lié va la table de jointure à une ou plusieurs formations.
- User : on a crée une table dans le cas de gestion des utilisateurs connectés sur la plateforme.

Les utilisateurs / use case : 
On a imaginé que les administrateurs, avec le + de droits, pouvaient créer, consulter, modifier et supprimer les données de toutes les tables.
Les utilisateurs connectés (élèves, formateurs) peuvent consulter les données des salles et élèves. 
Et un cas d'utilsateur non connecté ou on peut uniquement consuler les informations des formations. 

En plus du CRUD, nous avons ajouté ces fonctionnalités :

showStudents() dans StudentRepository permet d'afficher la liste d'étudiants d'une formation. 

findTeachersByCourse() dans CourseRepository permet d'afficher la liste des formateurs d'une formation. 

findRoomByCourse() dans CourseRepository permet de trouver la salle d'une formation.

findCourseByTeacher() dans TeacherRepository permet de trouver le(s) formation(s) du formateur indiqué.

findCourseByStudent() dans StudentRepository permet de trouver la formation de l'étudiant indiqué.

