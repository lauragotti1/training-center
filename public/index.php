<?php

use App\Entities\Room;
use App\Entities\User;
use App\Repository\RoomRepository;
use App\Entities\Teacher;
use App\Repository\StudentRepository;
use App\Entities\Course;
use App\Repository\CourseRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserRepository;
use DateTime;
require '../vendor/autoload.php';


//STUDENTREPO 
$studentrepo = new StudentRepository();
// var_dump($studentrepo->findAll());
// var_dump($studentrepo->showStudents(1));
var_dump($studentrepo->findCourseByStudent(1));

//COURSEREPO
$courserepo = new CourseRepository();
// var_dump($courserepo->findAll());
// $course = new Course('Nom formation', 'Description formation', new DateTime('2023-01-01'), new DateTime('2023-09-01'), 1);
// $courserepo->persist($course);
// var_dump($courserepo->findAll());
// var_dump($courserepo->findRoomByCourse(1));
var_dump($courserepo->findTeachersByCourse(2));



//ROOMREPO
$roomrepo = new RoomRepository();
// var_dump(($roomrepo->findAll()));
// $roomrepo->deleteById(2);
// var_dump(($roomrepo->findAll()));
// $roomrepo->update(new Room('symfony', 1));
// var_dump(($roomrepo->findAll()));


//TEACHERREPO
$teacherrepo = new TeacherRepository();
//$teacher = new Teacher('Gérard', 'Jugnot', 'gégéjuju@gmail.com');
// $teacherrepo->persist($teacher);
// var_dump($teacherrepo->findAll());
// $teacher = new Teacher('Test', 'update', 'mb@mail.com', 1);
// $repoTeacher->update($teacher)
// var_dump($teacherrepo->findCourseByTeacher(1));


//USERREPO
$userrepo = new UserRepository();
// $user = new User('glglg@gmail.com', 'password1234', 'User');
// $userrepo->persist($user);
// var_dump($userrepo->findAll());
