<?php


namespace App\Entities;
use DateTime;



class Course{

    private ?int $id;

    private string $name;

    private string $description;

    private DateTime $start;

    private DateTime $end;
	
	private int $id_room;

    /**
     * @param int $id
     * @param string $name
     * @param string $description
     * @param DateTime $start
     * @param DateTime $end
     */
    public function __construct(string $name, string $description, DateTime $start, DateTime $end, int $id_room, ?int $id = null) {
    	$this->id = $id;
    	$this->name = $name;
    	$this->description = $description;
    	$this->start = $start;
    	$this->end = $end;
		$this->id_room = $id_room;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}
	
	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getStart(): DateTime {
		return $this->start;
	}
	
	/**
	 * @param DateTime $start 
	 * @return self
	 */
	public function setStart(DateTime $start): self {
		$this->start = $start;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getEnd(): DateTime {
		return $this->end;
	}
	
	/**
	 * @param DateTime $end 
	 * @return self
	 */
	public function setEnd(DateTime $end): self {
		$this->end = $end;
		return $this;
	}

	

	/**
	 * @return int
	 */
	public function getId_room(): int {
		return $this->id_room;
	}
	
	/**
	 * @param int $id_room 
	 * @return self
	 */
	public function setId_room(int $id_room): self {
		$this->id_room = $id_room;
		return $this;
	}
}