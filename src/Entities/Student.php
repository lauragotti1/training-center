<?php

namespace App\Entities;

class Student{

	private ?int $id;
	private string $firstName;
	private string $lastName;
	private string $mail;
	private int $id_course;


	/**
	 * @param int|null $id
	 * @param string $firstName
	 * @param string $lastName
	 * @param string $mail
	 */
	public function __construct(string $firstName, string $lastName, string $mail, int $id_course, ?int $id = null)
	{
		$this->id = $id;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->mail = $mail;
		$this->id_course = $id_course;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFirstName(): string
	{
		return $this->firstName;
	}

	/**
	 * @param string $firstName 
	 * @return self
	 */
	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName 
	 * @return self
	 */
	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMail(): string
	{
		return $this->mail;
	}

	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self
	{
		$this->mail = $mail;
		return $this;
	}




	/**
	 * @return int
	 */
	public function getId_course(): int
	{
		return $this->id_course;
	}

	/**
	 * @param int $id_course 
	 * @return self
	 */
	public function setId_course(int $id_course): self
	{
		$this->id_course = $id_course;
		return $this;
	}
}