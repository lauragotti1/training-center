<?php

namespace App\Entities;


class Teacher{

    private ?int $id;

    private string $firstname;

    private string $lastname;

    private string $mail;



    /**
     * @param int|null $id
     * @param string $firstname
     * @param string $lastname
     * @param string $mail
     */
    public function __construct(string $firstname, string $lastname, string $mail,?int $id=null) {
    	$this->id = $id;
    	$this->firstname = $firstname;
    	$this->lastname = $lastname;
    	$this->mail = $mail;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLastname(): string {
		return $this->lastname;
	}
	
	/**
	 * @param string $lastname 
	 * @return self
	 */
	public function setLastname(string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}
}