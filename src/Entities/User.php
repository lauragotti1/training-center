<?php

namespace App\Entities;

class User{

    private ?int $id;
    private string $mail;
    private string $password;

    private string $role;

    /**
     * @param int|null $id
     * @param string $mail
     * @param string $password
     * @param string $role
     */
    public function __construct( string $mail, string $password, string $role,?int $id=null) {
    	$this->id = $id;
    	$this->mail = $mail;
    	$this->password = $password;
    	$this->role = $role;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}
	
	/**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRole(): string {
		return $this->role;
	}
	
	/**
	 * @param string $role 
	 * @return self
	 */
	public function setRole(string $role): self {
		$this->role = $role;
		return $this;
	}
}