<?php

namespace App\Repository;

use App\Entities\Course;
use DateTime;
use PDO;


class CourseRepository
{

    private PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=training_center', 'simplon', '1234');

    }

    public function findAll()
    {
        $course = [];
        $statement = $this->connection->prepare('SELECT * FROM course');
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $line) {
            $course[] = $line;

        }
        return $course;
    }


    public function persist(Course $course)
    {
        $statement = $this->connection->prepare('INSERT INTO course (name, description, start, end) VALUES (:name, :description, :start, :end)');
        $statement->execute([
            'name' => $course->getName(),
            'description' => $course->getDescription(),
            'start' => $course->getStart()->format('Y-m-d'),
            'end' => $course->getEnd()->format('Y-m-d')
        ]);
        $course->setId($this->connection->lastInsertId());
    }


    public function update(Course $course): void
    {
        $statement = $this->connection->prepare('UPDATE course SET name=:name, description=:description,start=:start, end=:end, id_room=:id_room WHERE id=:id');
        $statement->bindValue(":name", $course->getName(), PDO::PARAM_STR);
        $statement->bindValue(":description", $course->getDescription(), PDO::PARAM_STR);
        $statement->bindValue(":start", $course->getStart()->format('Y-m-d'));
        $statement->bindValue(":end", $course->getEnd()->format('Y-m-d'));
        $statement->bindValue(":id_room", $course->getId_room(), PDO::PARAM_INT);
        $statement->bindValue(':id', $course->getId(), PDO::PARAM_INT);

        $statement->execute();

    }


    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare('DELETE FROM course WHERE id = :id');
        $statement->execute([
            'id' => $id
        ]);
    }

    /**
     * Summary of findRoomByCourse
     * @param int $id de la course pour laquelle on veut connaitre la salle
     * @return 
     */

    public function findRoomByCourse(int $id)
    {
        $courses = [];
        $statement = $this->connection->prepare('SELECT * FROM course JOIN room r ON id_room = r.id WHERE r.id = :id');

        $statement->execute([
            'id' => $id
        ]);

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            // $courses[] = $item;
            // var_dump($item['name']);
        }

        return $item['name'];
    }


    /**
     * Summary of findTeachersByCourse
     * @param int $id = id de la formation pour laquelle on veut afficher les formateurs.
     * @return array
     */
    public function findTeachersByCourse(int $id)
    {
        $course = [];
        $statement = $this->connection->prepare('SELECT * FROM teacher LEFT JOIN course_teacher ON teacher.id = course_teacher.id_teacher WHERE course_teacher.id_course=:id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $line) {
            $course[] = $line;

        }
        return $course;
    }

    

    }




