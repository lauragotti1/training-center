<?php
namespace App\Repository;
use App\Entities\Room;
use PDO;



class RoomRepository
{

    private PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=training_center', 'simplon', '1234');

 
    }      
      public function findAll(){

        $rooms = [];
        $statement = $this->connection->prepare('SELECT * FROM room');

        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $rooms[] = $item;
        }
        return $rooms;
        }


        public function persist (Room $room) {
            $statement = $this->connection->prepare('INSERT INTO room (name) VALUES (:name)');
            $statement->execute([
                'name' => $room->getName(),
                    ]);
            $room->setId($this->connection->lastInsertId());
        }

        public function update(Room $room):void{
            $statement = $this->connection->prepare('UPDATE room SET name=:name WHERE id=:id');
            $statement->bindValue(":name", $room->getName(),PDO::PARAM_STR);
            $statement->bindValue(":id", $room->getId(),PDO::PARAM_INT);
    
            $statement -> execute();
    
        }

        public function deleteById (int $id) {
            $statement = $this->connection->prepare('DELETE FROM room WHERE id = :id');
            $statement->execute([
                'id' => $id
            ]);
        }

}