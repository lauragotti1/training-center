<?php
namespace App\Repository;

use PDO;
use App\Entities\Student;



class StudentRepository
{

    private PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=training_center', 'simplon', '1234');

    }

    public function findAll()
    {

        $students = [];
        $statement = $this->connection->prepare('SELECT * FROM student');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $students[] = $item;
        }
        return $students;
    }

    public function persist(Student $student)
    {
        $statement = $this->connection->prepare('INSERT INTO student (firstname, lastname, mail, id_course) VALUES (:firstname, :lastname, :mail, :id_course)');
        $statement->execute([
            'firstname' => $student->getFirstName(),
            'lastname' => $student->getLastName(),
            'mail' => $student->getMail(),
            'id_course' => $student->getId_course()
        ]);
        $student->setId($this->connection->lastInsertId());
    }

    public function update(Student $student): void
    {
        $statement = $this->connection->prepare('UPDATE student SET firstName=:firstName, lastName=:lastName, mail=:mail, id_course=:id_course WHERE id=:id');
        $statement->bindValue(":firstName", $student->getFirstName(), PDO::PARAM_STR);
        $statement->bindValue(":lastName", $student->getLastName(), PDO::PARAM_STR);
        $statement->bindValue(":mail", $student->getMail(), PDO::PARAM_STR);
        $statement->bindValue(":id_course", $student->getId_course(), PDO::PARAM_INT);
        $statement->bindValue(":id", $student->getId(), PDO::PARAM_INT);

        $statement->execute();

    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare('DELETE FROM student WHERE id = :id');
        $statement->execute([
            'id' => $id
        ]);
    }
    /**
     * *
     * @param int $id_course = l'id de la formation pour laquelle on veut afficher tous les étudiants 
     * @return array
     */
    public function showStudents(int $id_course)
    {
        $students = [];
        $statement = $this->connection->prepare('SELECT * FROM student WHERE id_course = :id_course');

        $statement->execute([
            'id_course' => $id_course
        ]);

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $students[] = $item;
        }
        return $students;
    }

    public function findCourseByStudent(int $id)
    {
        $student = [];
        $statement = $this->connection->prepare('SELECT * FROM student LEFT JOIN course ON id_course = course.id WHERE student.id =:id');
        $statement->execute([
            'id' => $id
        ]);

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            // $courses[] = $item;
            // var_dump($item['name']);

        }
        return $item;

    }
}