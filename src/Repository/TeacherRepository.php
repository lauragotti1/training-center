<?php

namespace App\Repository;

use App\Entities\Teacher;
use PDO;


class TeacherRepository
{

    private PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=training_center', 'simplon', '1234');

    }

    public function findAll()
    {
        $teacher = [];

        $statement = $this->connection->prepare('SELECT * FROM teacher');
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $line) {
            $teacher[] = $line;

        }
        return $teacher;

    }

    public function persist(Teacher $teacher)
    {
        $statement = $this->connection->prepare('INSERT INTO teacher (firstname, lastname, mail) VALUES (:firstname, :lastname, :mail)');
        $statement->execute([
            'firstname' => $teacher->getFirstName(),
            'lastname' => $teacher->getLastName(),
            'mail' => $teacher->getMail(),
        ]);

    }


    public function update(Teacher $teacher): void
    {
        $statement = $this->connection->prepare('UPDATE teacher SET firstName=:firstName, lastName=:lastName, mail=:mail WHERE id=:id');
        $statement->bindValue(":firstName", $teacher->getFirstName(), PDO::PARAM_STR);
        $statement->bindValue(":lastName", $teacher->getLastName(), PDO::PARAM_STR);
        $statement->bindValue(":mail", $teacher->getMail(), PDO::PARAM_STR);
        $statement->bindValue(":id", $teacher->getId(), PDO::PARAM_INT);

        $statement->execute();

    }

    public function delete(Teacher $teacher): void
    {
        $query = $this->connection->prepare("DELETE FROM teacher WHERE id=:id");
        $query->bindValue(":id", $teacher->getId(), PDO::PARAM_INT);
        $query->execute();
    }


    /**
     * Summary of findCourseByTeacher
     * @param int $id du teacher pour lequel on veut trouver ses courses
     * @return array
     */

    public function findCourseByTeacher(int $id)
    {
        $teacher = [];
        $statement = $this->connection->prepare('SELECT * FROM course LEFT JOIN course_teacher ON course.id = course_teacher.id_course WHERE course_teacher.id_teacher=:id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $line) {
            $teacher[] = $line;

        }
        return $teacher;
    }

}