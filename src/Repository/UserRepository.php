<?php
namespace App\Repository;
use App\Entities\User;
use PDO;



class UserRepository
{

    private PDO $connection;

    public function __construct()
    {

        $this->connection = new PDO('mysql:host=localhost;dbname=training_center', 'simplon', '1234');

 
    }      
      public function findAll(){

        $users = [];
        $statement = $this->connection->prepare('SELECT * FROM user');

        $statement->execute();
        
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $users[] = $item;
        }
        return $users;
        }


        public function persist (User $user) {
            $statement = $this->connection->prepare('INSERT INTO user (mail, password, role) VALUES (:mail, :password, :role)');
            $statement->execute([
                'mail' => $user->getMail(),
                'password' => $user->getPassword(),
                'role' => $user->getRole(),
        
            ]);
            $user->setId($this->connection->lastInsertId());
        }

        public function update(User $user):void{
            $statement = $this->connection->prepare('UPDATE user SET mail=:mail, password=:password, role=:role WHERE id=:id');
            $statement->bindValue(":mail", $user->getMail(),PDO::PARAM_STR);
            $statement->bindValue(":password", $user->getPassword(),PDO::PARAM_STR);
            $statement->bindValue(":role", $user->getRole(),PDO::PARAM_STR);
            $statement->bindValue(":id", $user->getId(),PDO::PARAM_INT);
    
            $statement -> execute();
    
        }

        public function deleteById (int $id) {
            $statement = $this->connection->prepare('DELETE FROM user WHERE id = :id');
            $statement->execute([
                'id' => $id
            ]);
        }

}