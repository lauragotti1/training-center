DROP TABLE IF EXISTS course_teacher;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS teacher;
DROP TABLE IF EXISTS user;

CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50)
);

CREATE TABLE course (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    description VARCHAR(500),
    start DATE,
    end DATE,
    id_room INT,
    FOREIGN KEY (id_room) REFERENCES room(id)
);

CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    mail VARCHAR(255),
    id_course INT,
    FOREIGN KEY (id_course) REFERENCES course(id)
);

CREATE TABLE teacher (
    id INT PRIMARY KEY AUTO_INCREMENT,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    mail VARCHAR(255)
);

CREATE TABLE user (
    id INT PRIMARY KEY AUTO_INCREMENT,
    mail VARCHAR(255),
    password VARCHAR(255),
    role VARCHAR(50)
);

CREATE TABLE course_teacher (
    PRIMARY KEY (id_course, id_teacher),
    id_course INT,  
    FOREIGN KEY (id_course) REFERENCES course(id),
    id_teacher INT,
    FOREIGN KEY (id_teacher) REFERENCES teacher(id)
);



INSERT INTO room (name) VALUES ('Salle Disney'), ('Salle MyCanal'), ('Salle PrimeVideo'), ('Salle Netflix'), ('Salle AppleTv');
INSERT INTO course (name, description, start, end, id_room) VALUES ('Disney', 'formation pour devenir Blanche neige ou les sept nains', '2022-11-21', '2023-09-06', 1), ('MyCanal', 'formation pour devenir Vincent Bolloré', '2022-10-01', '2023-08-25', 2),('PrimeVideo', 'formation pour devenir le plus gros con de la planète option:Univers','2023-01-01','2023-08_09',3),('Netflix','formation pour devenir extrême, soit être génial, soit être un gros navet, jamais de moyenne','2023-04-03','2023-05-12',4),('AppleTv','formation pour être au top tout le temps, lol','2023-09-01','2023-12_10',5);
INSERT INTO student (firstname, lastname, mail, id_course) VALUES ('Laura', 'Gotti', 'lg@mail.com', 1), ('Marie', 'Benitah', 'png@mail.com', 1), ('Nicolas', 'Harizia', 'nh@mail.com', 1),('Mélanie','Ferrer','ml@mail.com',2), ('Axel','Reviron','ar@mail.com',2), ('Hajar','Zahoui','hz@mail.com',2),('Juliette','Suc','js@mail.com',3),('Hisami','Stolz','hs@mail.com',3),('Yann','Pezavant','yp@mail.com',3),('Chems','Meziou','cm@mail.com',4),('Samantha','Grobon','sg@mail.com',4),('Alexandre','Noens','an@mail.com',4),('Ellyesse','Daoudi','ed@mail.com',5),('Laure','Glaizal','lg@mail.com',5),('Judith','Sévy','js@mail.com',5);
INSERT INTO teacher (firstname, lastname, mail) VALUES ('Gérard', 'Jugnot', 'gégéjuju@gmail.com'),('Thierry', 'Lermitte', 'titi@gmail.com'),('Jean-Claude', 'Duss', 'duduss@wanadoo.com'),('Josiane', 'Balasko', 'balaskas@gmail.com'),('Valerie', 'Lemercier', 'valoo@hotmail.fr');
INSERT INTO course_teacher(id_course, id_teacher) VALUES (1,1),(2,2),(1,2),(2,1),(3,3),(4,4),(4,3),(5,5),(5,4);
